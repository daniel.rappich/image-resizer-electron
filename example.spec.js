const { test, expect, _electron: electron } = require('@playwright/test')

test('example test', async () => {
	try {
		const electronApp = await electron.launch({
			args: ['./main.js'],
			viewport: { width: 1920, height: 1080 },
		})
		const isPackaged = await electronApp.evaluate(async ({ app }) => {
			// This runs in Electron's main process, parameter here is always
			// the result of the require('electron') in the main app script.
			return app.isPackaged
		})
		expect(isPackaged).toBe(false)

		// Wait for the first BrowserWindow to open
		// and return its Page object
		const window = await electronApp.firstWindow()

		// Set viewport size using Playwright's API within Electron's context
		await window.setViewportSize({ width: 1920, height: 1080 })

		// Wait briefly for the viewport to be set
		await window.waitForTimeout(1000)

		await window.screenshot({ path: 'test.png' })

		// close app
		await electronApp.close()
	} catch (err) {
		console.log(err)
	}
})
